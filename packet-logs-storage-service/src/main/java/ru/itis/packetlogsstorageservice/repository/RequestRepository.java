package ru.itis.packetlogsstorageservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.packetlogsstorageservice.model.Request;

import java.util.UUID;

public interface RequestRepository extends JpaRepository<Request, UUID> {
}
