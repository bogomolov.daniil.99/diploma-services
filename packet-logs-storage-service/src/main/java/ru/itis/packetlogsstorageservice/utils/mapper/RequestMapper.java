package ru.itis.packetlogsstorageservice.utils.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.itis.packetlogsstorageservice.dto.RequestLogDto;
import ru.itis.packetlogsstorageservice.model.Request;

@Mapper(componentModel = "spring")
public interface RequestMapper {

    Request toEntity(RequestLogDto requestLogDto);
}
