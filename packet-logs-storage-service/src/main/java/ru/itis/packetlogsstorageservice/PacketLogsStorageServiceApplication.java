package ru.itis.packetlogsstorageservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PacketLogsStorageServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(PacketLogsStorageServiceApplication.class, args);
    }

}
