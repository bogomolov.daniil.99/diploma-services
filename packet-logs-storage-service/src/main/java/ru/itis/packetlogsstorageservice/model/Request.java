package ru.itis.packetlogsstorageservice.model;

import lombok.*;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenericGenerator;
import ru.itis.packetlogsstorageservice.dto.LogType;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Request {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID requestId;

    private String tracertId;

    private String sourceAddress;

    private Integer sourcePort;

    private String destinationAddress;

    private Integer destinationPort;

    private LogType type;

    private String payload;
}
