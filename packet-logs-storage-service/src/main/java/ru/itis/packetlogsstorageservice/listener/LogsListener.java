package ru.itis.packetlogsstorageservice.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import ru.itis.packetlogsstorageservice.dto.RequestLogDto;
import ru.itis.packetlogsstorageservice.service.RequestService;

@Component
@RequiredArgsConstructor
@RabbitListener(queues = "logs-queue")
public class LogsListener {

    private final ObjectMapper objectMapper;
    private final RequestService requestService;

    @SneakyThrows
    @RabbitHandler
    public void receiveLogs(String requestLog) {
        RequestLogDto requestLogDto = objectMapper.readValue(requestLog, RequestLogDto.class);
        String s = new String((byte[]) requestLogDto.getPayload().getPayload());
        requestService.uploadRequestLog(requestLogDto);
    }
}
