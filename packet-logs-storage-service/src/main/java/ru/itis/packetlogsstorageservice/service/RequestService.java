package ru.itis.packetlogsstorageservice.service;

import ru.itis.packetlogsstorageservice.dto.RequestLogDto;

public interface RequestService {

    void uploadRequestLog(RequestLogDto logDto);
}
