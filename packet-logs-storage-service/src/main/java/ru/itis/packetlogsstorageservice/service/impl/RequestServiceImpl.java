package ru.itis.packetlogsstorageservice.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.packetlogsstorageservice.dto.RequestLogDto;
import ru.itis.packetlogsstorageservice.model.Request;
import ru.itis.packetlogsstorageservice.repository.RequestRepository;
import ru.itis.packetlogsstorageservice.service.RequestService;
import ru.itis.packetlogsstorageservice.utils.mapper.RequestMapper;

@Service
@RequiredArgsConstructor
public class RequestServiceImpl implements RequestService {

    private final RequestMapper requestMapper;
    private final RequestRepository requestRepository;

    @Override
    public void uploadRequestLog(RequestLogDto logDto) {
        Request request = requestMapper.toEntity(logDto);
        requestRepository.save(request);
    }
}
