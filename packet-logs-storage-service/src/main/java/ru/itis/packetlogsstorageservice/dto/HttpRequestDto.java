package ru.itis.packetlogsstorageservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.InputStream;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class HttpRequestDto {

    private String method;

    private String uri;

    private Integer code;

    private Map<String, String> headers;

    private Object payload;
}
