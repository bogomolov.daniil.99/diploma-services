package ru.itis.packetlogsstorageservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RequestLogDto {

    private String sourceAddress;

    private Integer sourcePort;

    private String destinationAddress;

    private Integer destinationPort;

    private LogType type;

    private HttpRequestDto payload;
}
