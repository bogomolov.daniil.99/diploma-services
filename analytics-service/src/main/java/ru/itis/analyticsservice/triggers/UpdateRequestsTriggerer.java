package ru.itis.analyticsservice.triggers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import ru.itis.analyticsservice.config.properties.StorageServiceProperties;
import ru.itis.analyticsservice.dto.request.GetAllRequestsDto;
import ru.itis.analyticsservice.dto.request.MicroserviceDto;
import ru.itis.analyticsservice.dto.response.RequestLogDto;
import ru.itis.analyticsservice.model.Project;
import ru.itis.analyticsservice.service.ProjectService;
import ru.itis.analyticsservice.service.RequestService;
import ru.itis.analyticsservice.utils.mapper.MicroserviceMapper;

import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class UpdateRequestsTriggerer {

    private final StorageServiceProperties storageServiceProperties;
    private final RestTemplate restTemplate;
    private final ProjectService projectService;
    private final RequestService requestService;
    private final MicroserviceMapper microserviceMapper;

    @Scheduled(cron = "0 0 0 * * *")
    public void getRequests() {
        List<Project> projects = projectService.getAllProjects();
        for (Project project : projects) {
            List<MicroserviceDto> microservices = project.getInvolvedMicroservices().stream()
                    .map(microserviceMapper::toDto)
                    .collect(Collectors.toList());
            List<RequestLogDto> requests = getAllRequestsFromStorage(microservices);
            requestService.saveOrUpdate(requests);
        }
    }

    private List<RequestLogDto> getAllRequestsFromStorage(List<MicroserviceDto> microservices) {
        HttpEntity<GetAllRequestsDto> request = new HttpEntity<>(
                new GetAllRequestsDto(microservices)
        );
        URI requestURI = URI.create(storageServiceProperties.getGetRequestsUrl());
        ResponseEntity<RequestLogDto[]> response = restTemplate.postForEntity(requestURI, request, RequestLogDto[].class);
        if (response.getStatusCode().is2xxSuccessful()) {
            RequestLogDto[] body = response.getBody();
            if (Objects.isNull(body)) {
                return Collections.emptyList();
            } else {
                return Arrays.asList(body);
            }
        }
        return Collections.emptyList();
    }

}
