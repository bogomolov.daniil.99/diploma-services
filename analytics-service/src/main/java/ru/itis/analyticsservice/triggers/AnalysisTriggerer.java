package ru.itis.analyticsservice.triggers;

import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.annotation.Schedules;
import org.springframework.stereotype.Component;
import ru.itis.analyticsservice.model.Project;
import ru.itis.analyticsservice.service.AnalysisService;
import ru.itis.analyticsservice.service.ProjectService;

import java.util.List;

@Component
@RequiredArgsConstructor
public class AnalysisTriggerer {

    private final ProjectService projectService;
    private final List<AnalysisService> analysisServices;

    @Scheduled(fixedDelay = 60 * 60 * 1000)
    public void startAnalysis() {
        List<Project> projectsToAnalyse = projectService.getAllProjects();
        for (Project project : projectsToAnalyse) {
            analysisServices.forEach(service -> service.analyseProject(project));
        }
    }
}
