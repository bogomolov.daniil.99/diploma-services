package ru.itis.analyticsservice.utils.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.itis.analyticsservice.dto.response.RequestLogDto;
import ru.itis.analyticsservice.model.Request;

@Mapper(componentModel = "spring")
public interface RequestMapper {

    @Mapping(target = "fromService", ignore = true)
    @Mapping(target = "toService", ignore = true)
    Request toEntity(RequestLogDto requestLogDto);
}
