package ru.itis.analyticsservice.utils.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.itis.analyticsservice.model.Microservice;
import ru.itis.analyticsservice.model.recommendation.Recommendation;
import ru.itis.analyticsservice.model.recommendation.RecommendationType;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RecommendationMapper {

    Recommendation toEntity(RecommendationType type, List<Microservice> involvedServices);
}
