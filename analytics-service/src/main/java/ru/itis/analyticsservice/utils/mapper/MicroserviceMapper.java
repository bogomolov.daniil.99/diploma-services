package ru.itis.analyticsservice.utils.mapper;

import org.mapstruct.Mapper;
import ru.itis.analyticsservice.dto.request.MicroserviceDto;
import ru.itis.analyticsservice.model.Microservice;

@Mapper(componentModel = "spring")
public interface MicroserviceMapper {

    MicroserviceDto toDto(Microservice microservice);
}
