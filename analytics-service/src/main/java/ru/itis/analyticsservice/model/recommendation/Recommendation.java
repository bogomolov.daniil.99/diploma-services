package ru.itis.analyticsservice.model.recommendation;

import lombok.*;
import lombok.experimental.SuperBuilder;
import ru.itis.analyticsservice.model.AbstractEntity;
import ru.itis.analyticsservice.model.Microservice;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToMany;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Entity
public class Recommendation extends AbstractEntity {

    @ManyToMany
    private List<Microservice> involvedMicroservices;

    @Enumerated(EnumType.STRING)
    private RecommendationType type;

    @Builder.Default
    @Enumerated(EnumType.STRING)
    private RecommendationState state = RecommendationState.CREATED;
}
