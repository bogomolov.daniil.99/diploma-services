package ru.itis.analyticsservice.model.recommendation;

public enum RecommendationType {
    CYCLIC,
    HIGH_COUPLING,
    LONG_TIME_USAGE
}
