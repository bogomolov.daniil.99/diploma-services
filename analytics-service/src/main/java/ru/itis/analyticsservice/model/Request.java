package ru.itis.analyticsservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder(toBuilder = true)
@Entity
public class Request extends AbstractEntity {

    private String tracerouteId;

    private String uri;

    private String code;

    private String methodName;

    @Enumerated(EnumType.STRING)
    private RequestType type;

    @ManyToOne
    @JoinColumn(name = "from_service_id")
    private Microservice fromService;

    @ManyToOne
    @JoinColumn(name = "to_service_id")
    private Microservice toService;

    private LocalDateTime sendDateTime;

    public boolean equalsByRequestMetadata(Object other) {
        if (this == other) return true;
        if (other == null || getClass() != other.getClass()) return false;
        Request otherRequest = (Request) other;
        return Objects.equals(type, otherRequest.getType())
                && Objects.equals(methodName, otherRequest.getMethodName())
                && Objects.equals(fromService.getAddress(), otherRequest.getFromService().getAddress())
                && Objects.equals(fromService.getPort(), otherRequest.getFromService().getPort())
                && Objects.equals(toService.getAddress(), otherRequest.getToService().getAddress())
                && Objects.equals(toService.getPort(), otherRequest.getToService().getPort());
    }
}
