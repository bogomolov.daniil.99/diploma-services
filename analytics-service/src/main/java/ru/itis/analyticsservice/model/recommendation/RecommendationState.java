package ru.itis.analyticsservice.model.recommendation;

public enum RecommendationState {
    CREATED,
    ACCEPTED,
    DECLINED
}
