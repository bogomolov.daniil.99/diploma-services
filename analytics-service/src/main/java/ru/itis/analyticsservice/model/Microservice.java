package ru.itis.analyticsservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Entity
public class Microservice extends AbstractEntity {

    private String name;

    private String address;

    private int port;

    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;
}
