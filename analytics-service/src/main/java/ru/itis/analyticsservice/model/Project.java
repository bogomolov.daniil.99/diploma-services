package ru.itis.analyticsservice.model;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Entity
public class Project extends AbstractEntity {

    private String name;

    @Builder.Default
    @OneToMany(mappedBy = "project")
    private Set<Microservice> involvedMicroservices = new HashSet<>();

    @Builder.Default
    @ManyToMany
    @JoinTable(name = "project_chain")
    private Set<RequestChain> requestChains = new HashSet<>();

    @OneToOne
    @JoinColumn(name = "settings_id")
    private ProjectSettings settings;
}
