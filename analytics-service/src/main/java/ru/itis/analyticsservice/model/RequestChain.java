package ru.itis.analyticsservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.*;
import java.util.stream.Collectors;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Entity
public class RequestChain extends AbstractEntity {

    private String tracerouteId;

    @ManyToMany
    private Set<Request> requests = new HashSet<>();

    public boolean equalsByRequests(Object other) {
        if (this == other) return true;
        if (other == null || getClass() != other.getClass()) return false;
        RequestChain otherChain = (RequestChain) other;

        int thisRequestsCount = requests.size();
        int otherRequestsCount = otherChain.getRequests().size();
        if (thisRequestsCount != otherRequestsCount) return false;

        List<Request> thisRequestsOrdered = getRequestsOrderedByCreateDate();
        List<Request> otherRequestsOrdered = otherChain.getRequestsOrderedByCreateDate();

        for (int i = 0; i < thisRequestsCount; i++) {
            Request thisRequest = thisRequestsOrdered.get(i);
            Request otherRequest = otherRequestsOrdered.get(i);
            if (!thisRequest.equalsByRequestMetadata(otherRequest)) {
                return false;
            }
        }

        return true;
    }

    public List<Request> getRequestsOrderedByCreateDate() {
        return requests.stream()
                .sorted(Comparator.comparing(Request::getSendDateTime))
                .toList();
    }

}