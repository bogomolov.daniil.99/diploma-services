package ru.itis.analyticsservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Entity
public class ProjectSettings extends AbstractEntity {

    private int maxResponseTimeMillis;

    private int maxMicroserviceCouplingPercentage;
}
