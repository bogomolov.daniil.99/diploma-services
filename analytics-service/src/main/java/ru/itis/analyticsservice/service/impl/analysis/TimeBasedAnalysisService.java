package ru.itis.analyticsservice.service.impl.analysis;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.analyticsservice.config.properties.AnalysisProperties;
import ru.itis.analyticsservice.model.Microservice;
import ru.itis.analyticsservice.model.Project;
import ru.itis.analyticsservice.model.Request;
import ru.itis.analyticsservice.model.RequestChain;
import ru.itis.analyticsservice.model.recommendation.RecommendationType;
import ru.itis.analyticsservice.service.AnalysisService;
import ru.itis.analyticsservice.service.RecommendationService;

import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class TimeBasedAnalysisService implements AnalysisService {

    private final AnalysisProperties analysisProperties;
    private final RecommendationService recomendationService;

    @Override
    public void analyseProject(Project project) {
        for (RequestChain chain : project.getRequestChains()) {
            if (!isResponseTakesTooLong(chain)) {
                continue;
            }
            List<Microservice> involvedMicroservices = chain.getRequests().stream()
                    .flatMap(request -> Stream.of(request.getFromService(), request.getToService()))
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
            recomendationService.createRecommendation(RecommendationType.LONG_TIME_USAGE, involvedMicroservices);
        }
    }

    private boolean isResponseTakesTooLong(RequestChain requestChain) {
        List<Request> requestsOrderedByTime = requestChain.getRequestsOrderedByCreateDate();
        Request firstRequest = requestsOrderedByTime.get(0);
        Request lastRequest = requestsOrderedByTime.get(requestsOrderedByTime.size() - 1);
        return ChronoUnit.MILLIS.between(firstRequest.getSendDateTime(), lastRequest.getSendDateTime()) < analysisProperties.getMaxResponseTimeMillis();
    }
}
