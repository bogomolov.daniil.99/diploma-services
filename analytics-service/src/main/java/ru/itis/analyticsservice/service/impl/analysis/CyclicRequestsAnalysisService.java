package ru.itis.analyticsservice.service.impl.analysis;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.analyticsservice.model.*;
import ru.itis.analyticsservice.model.recommendation.RecommendationType;
import ru.itis.analyticsservice.service.AnalysisService;
import ru.itis.analyticsservice.service.RecommendationService;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CyclicRequestsAnalysisService implements AnalysisService {

    private final RecommendationService recomendationService;

    @Override
    public void analyseProject(Project project) {
        for (RequestChain chain : project.getRequestChains()) {
            List<Microservice> cycleInChain = findCycleInChain(chain);
            if (!isRequestChainCyclic(cycleInChain)) {
                continue;
            }
            recomendationService.createRecommendation(RecommendationType.CYCLIC, cycleInChain);
        }
    }

    private List<Microservice> findCycleInChain(RequestChain chain) {
        int from = 0;
        int to = 0;
        List<Microservice> microserviceRequestChain = getMicroserviceRequestChain(chain);
        for (int i = 0; i < microserviceRequestChain.size(); i++) {
            Microservice microservice = microserviceRequestChain.get(i);
            int firstOccurrence = microserviceRequestChain.indexOf(microservice);
            int lastOccurrence = microserviceRequestChain.lastIndexOf(microservice);
            if (lastOccurrence - firstOccurrence >= to - from) {
                from = firstOccurrence;
                to = lastOccurrence;
            }
        }
        return microserviceRequestChain.subList(from, to);
    }

    private List<Microservice> getMicroserviceRequestChain(RequestChain chain) {
        return chain.getRequests()
                .stream()
                .filter(request -> RequestType.REQUEST.equals(request.getType()))
                .sorted(Comparator.comparing(Request::getSendDateTime))
                .map(Request::getToService)
                .toList();
    }

    private boolean isRequestChainCyclic(List<Microservice> microserviceChain) {
        return microserviceChain.size() > 1;
    }
}
