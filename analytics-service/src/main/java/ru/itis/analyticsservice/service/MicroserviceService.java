package ru.itis.analyticsservice.service;

import ru.itis.analyticsservice.model.Microservice;

public interface MicroserviceService {

    Microservice getOrSave(String address, Integer port);
}
