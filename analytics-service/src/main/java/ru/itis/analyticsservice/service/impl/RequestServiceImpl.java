package ru.itis.analyticsservice.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.analyticsservice.dto.response.RequestLogDto;
import ru.itis.analyticsservice.model.Microservice;
import ru.itis.analyticsservice.model.Request;
import ru.itis.analyticsservice.repository.MicroserviceRepository;
import ru.itis.analyticsservice.repository.RequestRepository;
import ru.itis.analyticsservice.service.MicroserviceService;
import ru.itis.analyticsservice.service.RequestService;
import ru.itis.analyticsservice.utils.mapper.RequestMapper;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RequestServiceImpl implements RequestService {

    private final MicroserviceService microserviceService;
    private final RequestRepository requestRepository;
    private final RequestMapper requestMapper;

    @Override
    @Transactional
    public void saveOrUpdate(List<RequestLogDto> requests) {
        List<Request> newRequests = new ArrayList<>();
        for (RequestLogDto requestDto : requests) {
            Request requestEntity = requestMapper.toEntity(requestDto).toBuilder()
                    .fromService(microserviceService.getOrSave(requestDto.getSourceAddress(), requestDto.getSourcePort()))
                    .toService(microserviceService.getOrSave(requestDto.getDestinationAddress(), requestDto.getDestinationPort()))
                    .build();
            requestRepository.findByUriAndMethodNameAndFromServiceAndToService(
                    requestEntity.getUri(), requestEntity.getMethodName(),
                    requestEntity.getFromService(), requestEntity.getToService()
            ).ifPresent(request -> requestEntity.setId(request.getId()));
            newRequests.add(requestEntity);
        }
        requestRepository.saveAll(newRequests);
    }
}
