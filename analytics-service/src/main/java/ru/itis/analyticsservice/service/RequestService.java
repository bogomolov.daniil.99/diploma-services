package ru.itis.analyticsservice.service;

import ru.itis.analyticsservice.dto.response.RequestLogDto;

import java.util.List;

public interface RequestService {
    void saveOrUpdate(List<RequestLogDto> requests);
}
