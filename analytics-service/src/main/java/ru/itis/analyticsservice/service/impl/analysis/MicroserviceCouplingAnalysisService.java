package ru.itis.analyticsservice.service.impl.analysis;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.analyticsservice.config.properties.AnalysisProperties;
import ru.itis.analyticsservice.model.Microservice;
import ru.itis.analyticsservice.model.Project;
import ru.itis.analyticsservice.model.Request;
import ru.itis.analyticsservice.model.RequestChain;
import ru.itis.analyticsservice.model.recommendation.RecommendationType;
import ru.itis.analyticsservice.service.AnalysisService;
import ru.itis.analyticsservice.service.RecommendationService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class MicroserviceCouplingAnalysisService implements AnalysisService {

    private final AnalysisProperties analysisProperties;
    private final RecommendationService recommendationService;

    @Override
    public void analyseProject(Project project) {
        int uniqueRequestsCount = getNumberOfUniqueRequests(project);
        for (Microservice microservice : project.getInvolvedMicroservices()) {
            int uniqueRequestsMicroserviceInvolvedCount =
                    getNumberOfUniqueRequestsMicroserviceInvolved(project, microservice);
            double couplingPercentage = (double)uniqueRequestsMicroserviceInvolvedCount / uniqueRequestsCount;
            if (couplingPercentage > analysisProperties.getMaxCouplingPercentage()) {
                recommendationService.createRecommendation(RecommendationType.HIGH_COUPLING, List.of(microservice));
            }
        }
    }

    private List<RequestChain> getUniqueRequests(Project project) {
        Set<RequestChain> requestChains = project.getRequestChains();
        List<RequestChain> uniqueChains = new ArrayList<>();
        for (RequestChain chain : requestChains) {
            boolean isChainNotInUniqueList = uniqueChains.stream()
                    .noneMatch(chain::equalsByRequests);
            if (isChainNotInUniqueList) {
                uniqueChains.add(chain);
            }
        }
        return uniqueChains;
    }

    private int getNumberOfUniqueRequests(Project project) {
        return getUniqueRequests(project).size();
    }

    private int getNumberOfUniqueRequestsMicroserviceInvolved(Project project, Microservice microservice) {
        List<RequestChain> uniqueRequests = getUniqueRequests(project);
        int countInvolved = 0;
        for (RequestChain requestChain : uniqueRequests) {
            Set<Request> requests = requestChain.getRequests();
            boolean isMicroserviceInvolvedInChain = requests.stream().
                    anyMatch(request -> microservice.equals(request.getFromService()) || microservice.equals(request.getToService()));
            if (isMicroserviceInvolvedInChain) {
                countInvolved++;
            }
        }
        return countInvolved;
    }
}
