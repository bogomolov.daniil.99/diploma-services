package ru.itis.analyticsservice.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.analyticsservice.model.Microservice;
import ru.itis.analyticsservice.repository.MicroserviceRepository;
import ru.itis.analyticsservice.service.MicroserviceService;

@Service
@RequiredArgsConstructor
public class MicroserviceServiceImpl implements MicroserviceService {

    private final MicroserviceRepository microserviceRepository;

    @Override
    public Microservice getOrSave(String address, Integer port) {
        return microserviceRepository.findByAddressAndPort(address, port)
                .orElseGet(() -> microserviceRepository.save(
                        Microservice.builder()
                                .address(address)
                                .port(port)
                                .build()
                ));
    }
}
