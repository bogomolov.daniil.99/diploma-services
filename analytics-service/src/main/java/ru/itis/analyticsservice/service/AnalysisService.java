package ru.itis.analyticsservice.service;

import ru.itis.analyticsservice.model.Project;

public interface AnalysisService {

    void analyseProject(Project project);
}
