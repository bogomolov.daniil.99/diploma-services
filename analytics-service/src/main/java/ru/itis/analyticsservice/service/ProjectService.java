package ru.itis.analyticsservice.service;

import ru.itis.analyticsservice.model.Project;

import java.util.List;

public interface ProjectService {

    List<Project> getAllProjects();
}
