package ru.itis.analyticsservice.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.analyticsservice.model.Microservice;
import ru.itis.analyticsservice.model.recommendation.Recommendation;
import ru.itis.analyticsservice.model.recommendation.RecommendationType;
import ru.itis.analyticsservice.repository.RecommendationRepository;
import ru.itis.analyticsservice.service.RecommendationService;
import ru.itis.analyticsservice.utils.mapper.RecommendationMapper;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RecommendationServiceImpl implements RecommendationService {

    private final RecommendationRepository recommendationRepository;
    private final RecommendationMapper recommendationMapper;

    @Override
    public void createRecommendation(RecommendationType type, List<Microservice> involvedServices) {
        Recommendation recommendation = recommendationMapper.toEntity(type, involvedServices);
        recommendationRepository.save(recommendation);
    }
}
