package ru.itis.analyticsservice.service;

import ru.itis.analyticsservice.model.Microservice;
import ru.itis.analyticsservice.model.recommendation.RecommendationType;

import java.util.List;

public interface RecommendationService {

    void createRecommendation(RecommendationType type, List<Microservice> involvedServices);
}
