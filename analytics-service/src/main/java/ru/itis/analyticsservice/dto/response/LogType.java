package ru.itis.analyticsservice.dto.response;

import java.util.Objects;

public enum LogType {
    REQUEST,
    RESPONSE
}
