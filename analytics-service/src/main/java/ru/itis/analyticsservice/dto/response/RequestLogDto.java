package ru.itis.analyticsservice.dto.response;

import lombok.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RequestLogDto {

    private String tracerouteId;

    private String sourceAddress;

    private Integer sourcePort;

    private String destinationAddress;

    private Integer destinationPort;

    private LogType type;

    private HttpRequestDto payload;

}
