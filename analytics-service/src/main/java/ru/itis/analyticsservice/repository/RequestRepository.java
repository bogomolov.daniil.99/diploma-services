package ru.itis.analyticsservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.analyticsservice.model.Microservice;
import ru.itis.analyticsservice.model.Request;

import java.util.Optional;
import java.util.UUID;

public interface RequestRepository extends JpaRepository<Request, UUID> {

    Optional<Request> findByUriAndMethodNameAndFromServiceAndToService(String uri, String method, Microservice from, Microservice to);
}
