package ru.itis.analyticsservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.analyticsservice.model.Project;

import java.util.UUID;

public interface ProjectRepository extends JpaRepository<Project, UUID> {
}
