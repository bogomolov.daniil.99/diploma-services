package ru.itis.analyticsservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.analyticsservice.model.recommendation.Recommendation;

import java.util.UUID;

public interface RecommendationRepository extends JpaRepository<Recommendation, UUID> {
}
