package ru.itis.analyticsservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.analyticsservice.model.Microservice;

import java.util.Optional;
import java.util.UUID;

public interface MicroserviceRepository extends JpaRepository<Microservice, UUID> {

    Optional<Microservice> findByAddressAndPort(String address, Integer port);
}
