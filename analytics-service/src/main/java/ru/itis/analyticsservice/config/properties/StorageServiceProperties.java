package ru.itis.analyticsservice.config.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "ru.itis.analyticsservice.storage-service")
public class StorageServiceProperties {

    private String baseUrl;

    private String getRequestsUrl;
}
