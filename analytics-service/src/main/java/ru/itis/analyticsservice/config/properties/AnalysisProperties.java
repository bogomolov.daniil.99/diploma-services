package ru.itis.analyticsservice.config.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "ru.itis.analyticsservice.analysis-properties")
public class AnalysisProperties {

    private Integer maxResponseTimeMillis;

    private Integer maxCouplingPercentage;
}
