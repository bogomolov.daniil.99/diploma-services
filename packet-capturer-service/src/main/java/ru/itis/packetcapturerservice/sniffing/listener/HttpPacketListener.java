package ru.itis.packetcapturerservice.sniffing.listener;

import lombok.RequiredArgsConstructor;
import org.pcap4j.core.PacketListener;
import org.pcap4j.packet.*;
import org.springframework.stereotype.Component;
import ru.itis.packetcapturerservice.dto.RequestLog;
import ru.itis.packetcapturerservice.messaging.LogSenderService;
import ru.itis.packetcapturerservice.sniffing.parser.PacketParser;

import java.util.List;
import java.util.Objects;

@Component
@RequiredArgsConstructor
public class HttpPacketListener implements PacketListener {

    private final List<PacketParser> parsers;
    private final LogSenderService senderService;

    @Override
    public void gotPacket(Packet packet) {
        Packet wrappedPacket = packet.getPayload();
        parsers.stream()
                .filter(parser -> parser.isSupported(wrappedPacket))
                .findFirst()
                .map(parser -> parser.parsePacket(wrappedPacket))
                .filter(requestLog -> Objects.nonNull(requestLog.getPayload()))
                .ifPresent(senderService::sendLog);
    }
}
