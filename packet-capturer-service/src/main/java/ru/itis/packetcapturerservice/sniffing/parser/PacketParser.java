package ru.itis.packetcapturerservice.sniffing.parser;

import org.pcap4j.packet.Packet;
import ru.itis.packetcapturerservice.dto.RequestLog;

public interface PacketParser {

    RequestLog parsePacket(Packet packet);

    boolean isSupported(Packet packet);
}
