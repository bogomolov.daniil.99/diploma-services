package ru.itis.packetcapturerservice.sniffing.parser.impl;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.pcap4j.packet.IpV4Packet;
import org.pcap4j.packet.Packet;
import org.pcap4j.packet.TcpPacket;
import org.springframework.stereotype.Service;
import ru.itis.packetcapturerservice.config.SniffingProperties;
import ru.itis.packetcapturerservice.dto.LogType;
import ru.itis.packetcapturerservice.dto.RequestLog;
import ru.itis.packetcapturerservice.sniffing.parser.HttpParser;
import ru.itis.packetcapturerservice.sniffing.parser.PacketParser;

@Service
@RequiredArgsConstructor
public class Ipv4PacketParser implements PacketParser {

    private final SniffingProperties sniffingProperties;
    private final HttpParser httpParser;

    @SneakyThrows
    @Override
    public RequestLog parsePacket(Packet abstractPacket) {
        IpV4Packet packet = (IpV4Packet) abstractPacket;
        IpV4Packet.IpV4Header ipV4Header = packet.getHeader();
        TcpPacket.TcpHeader tcpHeader = (TcpPacket.TcpHeader) packet.getPayload().getHeader();
        Packet tcpPayload = packet.getPayload().getPayload();

        Integer sourcePort = tcpHeader.getSrcPort().valueAsInt();
        LogType httpLogType =
                LogType.getByApplicationAndRequestSourcePorts(sniffingProperties.getListenablePort(), sourcePort);

        return RequestLog.builder()
                .sourceAddress(ipV4Header.getSrcAddr().toString())
                .sourcePort(sourcePort)
                .destinationAddress(ipV4Header.getDstAddr().toString())
                .destinationPort(tcpHeader.getDstPort().valueAsInt())
                .type(httpLogType)
                .payload(httpParser.parseRawHttpData(tcpPayload.getRawData(), httpLogType))
                .build();
    }

    @Override
    public boolean isSupported(Packet packet) {
        return packet instanceof IpV4Packet;
    }
}
