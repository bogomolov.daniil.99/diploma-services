package ru.itis.packetcapturerservice.sniffing.parser;

import org.apache.http.io.SessionInputBuffer;
import ru.itis.packetcapturerservice.dto.HttpRequestDto;
import ru.itis.packetcapturerservice.dto.LogType;

public interface HttpParser {

    HttpRequestDto parseRawHttpData(byte[] httpRequestData, LogType httpType);

    HttpRequestDto parseRawHttpRequest(SessionInputBuffer buffer);

    HttpRequestDto parseRawHttpResponse(SessionInputBuffer buffer);
}
