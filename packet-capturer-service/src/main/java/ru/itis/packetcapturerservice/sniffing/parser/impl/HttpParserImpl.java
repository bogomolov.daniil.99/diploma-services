package ru.itis.packetcapturerservice.sniffing.parser.impl;

import lombok.SneakyThrows;
import org.apache.http.Header;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.impl.io.DefaultHttpRequestParser;
import org.apache.http.impl.io.DefaultHttpResponseParser;
import org.apache.http.impl.io.HttpTransportMetricsImpl;
import org.apache.http.impl.io.SessionInputBufferImpl;
import org.apache.http.io.SessionInputBuffer;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import ru.itis.packetcapturerservice.dto.HttpRequestDto;
import ru.itis.packetcapturerservice.dto.LogType;
import ru.itis.packetcapturerservice.sniffing.parser.HttpParser;

import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.Map;

@Service
public class HttpParserImpl implements HttpParser {

    public static final String END_OF_HTTP_MESSAGE = "0";

    @Override
    public HttpRequestDto parseRawHttpData(byte[] httpRequestData, LogType httpType) {
        String requestString = new String(httpRequestData).replaceAll("\\r|\\n", "");
        if (!StringUtils.hasLength(requestString) || requestString.equals(END_OF_HTTP_MESSAGE)) {
            return null;
        }

        SessionInputBufferImpl buffer =
                new SessionInputBufferImpl(new HttpTransportMetricsImpl(), httpRequestData.length);
        buffer.bind(new ByteArrayInputStream(httpRequestData));

        if (LogType.REQUEST.equals(httpType)) {
            return parseRawHttpRequest(buffer);
        } else {
            return parseRawHttpResponse(buffer);
        }
    }

    @SneakyThrows
    @Override
    public HttpRequestDto parseRawHttpRequest(SessionInputBuffer buffer) {
        DefaultHttpRequestParser requestParser = new DefaultHttpRequestParser(buffer);
        HttpRequest request = requestParser.parse();

        return HttpRequestDto.builder()
                .method(request.getRequestLine().getMethod())
                .uri(request.getRequestLine().getUri())
                .headers(parseHeaders(request.getAllHeaders()))
                .build();
    }

    @SneakyThrows
    @Override
    public HttpRequestDto parseRawHttpResponse(SessionInputBuffer buffer) {
        DefaultHttpResponseParser responseParser = new DefaultHttpResponseParser(buffer);
        HttpResponse response = responseParser.parse();
        return HttpRequestDto.builder()
                .code(response.getStatusLine().getStatusCode())
                .headers(parseHeaders(response.getAllHeaders()))
                .payload(response.getEntity())
                .build();
    }

    private Map<String, String> parseHeaders(Header[] headers) {
        Map<String, String> headersMap = new HashMap<>();

        for (Header header : headers) {
            headersMap.put(header.getName(), header.getValue());
        }

        return headersMap;
    }

}