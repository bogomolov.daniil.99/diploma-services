package ru.itis.packetcapturerservice.dto;

import java.util.Objects;

public enum LogType {
    REQUEST,
    RESPONSE;

    public static LogType getByApplicationAndRequestSourcePorts(Integer applicationPort, Integer requestSourcePort) {
        if (Objects.equals(applicationPort, requestSourcePort)) {
            return RESPONSE;
        } else {
            return REQUEST;
        }
    }
}
