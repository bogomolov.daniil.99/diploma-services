package ru.itis.packetcapturerservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RequestLog {

    private String sourceAddress;

    private Integer sourcePort;

    private String destinationAddress;

    private Integer destinationPort;

    private LogType type;

    private HttpRequestDto payload;
}
