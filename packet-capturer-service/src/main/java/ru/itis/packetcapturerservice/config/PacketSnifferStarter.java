package ru.itis.packetcapturerservice.config;

import lombok.RequiredArgsConstructor;
import org.pcap4j.core.BpfProgram;
import org.pcap4j.core.PcapHandle;
import org.pcap4j.core.PcapNetworkInterface;
import org.pcap4j.core.Pcaps;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import ru.itis.packetcapturerservice.sniffing.listener.HttpPacketListener;

@Component
@RequiredArgsConstructor
public class PacketSnifferStarter implements CommandLineRunner {

    private final SniffingProperties properties;
    private final HttpPacketListener packetListener;

    @Override
    public void run(String... args) throws Exception {
        PcapNetworkInterface device = Pcaps.getDevByName(properties.getDefaultInterface());
        PcapHandle handler = device.openLive(
                properties.getMaxPacketSize(), PcapNetworkInterface.PromiscuousMode.PROMISCUOUS, properties.getTimeout()
        );
        handler.setFilter(properties.getBpfExpression(), BpfProgram.BpfCompileMode.OPTIMIZE);
        handler.loop(-1, packetListener);
    }

}
