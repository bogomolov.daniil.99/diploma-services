package ru.itis.packetcapturerservice.config;

import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BrokerConfiguration {

    @Bean
    public Queue queue(@Value("${application.messaging.rabbitmq.queue-name}") String queueName) {
        return new Queue(queueName);
    }
}
