package ru.itis.packetcapturerservice.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter
@Setter
@ConfigurationProperties(prefix = "application.sniffing")
public class SniffingProperties {

    private String defaultInterface;

    private Integer listenablePort;

    private Integer maxPacketSize;

    private Integer timeout;

    private String bpfExpression;
}
