package ru.itis.packetcapturerservice.messaging;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;
import ru.itis.packetcapturerservice.dto.RequestLog;

@Service
@RequiredArgsConstructor
public class LogToRabbitSenderService implements LogSenderService {

    private final RabbitTemplate rabbitTemplate;
    private final ObjectMapper objectMapper;
    private final Queue queue;

    @Override
    @SneakyThrows
    public void sendLog(RequestLog requestLog) {
        String logJson = objectMapper.writeValueAsString(requestLog);
        System.err.println("Putting: \n" + logJson);
        rabbitTemplate.convertAndSend(queue.getName(), logJson);
    }
}
