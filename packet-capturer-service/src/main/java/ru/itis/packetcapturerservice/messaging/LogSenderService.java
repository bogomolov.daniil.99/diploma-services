package ru.itis.packetcapturerservice.messaging;

import ru.itis.packetcapturerservice.dto.RequestLog;

public interface LogSenderService {

    void sendLog(RequestLog requestLog);
}
