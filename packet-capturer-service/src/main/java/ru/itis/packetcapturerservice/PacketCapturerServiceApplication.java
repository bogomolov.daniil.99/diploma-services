package ru.itis.packetcapturerservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PacketCapturerServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PacketCapturerServiceApplication.class, args);
	}

}
